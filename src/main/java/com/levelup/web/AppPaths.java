package com.levelup.web;

public class AppPaths {

    public static final String ADD_USER = "/addUser";
    public static final String CHANGE_USER = "/changeUser/{id}";
}
