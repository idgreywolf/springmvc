package com.levelup.web;

import com.levelup.web.dto.UserDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Map;

import static com.levelup.web.AppPaths.ADD_USER;
import static com.levelup.web.AppPaths.CHANGE_USER;

@Controller
public class MainController {

    Map<Integer, UserDTO> users = new HashMap();

    @RequestMapping(value = {"/","/home"}, method = RequestMethod.GET)
    public String getMain(Model model) {

        model.addAttribute("name","HGdjfjbhfjhbjsdjsjdbjks");
        model.addAttribute("lastname","ILYA");
        return "main_bootstrap.html";
    }

    @RequestMapping(value = ADD_USER, method = RequestMethod.GET)
    public String addUserForm(Model model) {
        model.addAttribute("user", new UserDTO());
        return "addUser.html";
    }


    @RequestMapping(value = ADD_USER, method = RequestMethod.POST)
    public String saveUser(UserDTO user, Model model) {
        user.setId(1);
        users.put(1, user);
        model.addAttribute("user", new UserDTO());
        return "addUser.html";
    }

    @RequestMapping(value = CHANGE_USER, method = RequestMethod.GET)
    public String changeUserForm(@PathVariable("id") int id, Model model) {
        UserDTO userDTO = users.get(id);
        model.addAttribute("user",userDTO);
        return "addUser.html";
    }




}
