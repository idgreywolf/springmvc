package com.levelup.web.dto;

import lombok.Data;

@Data
public class UserDTO {
    private int id;
    private String login;
    private String password;
}
